import React, { Component } from 'react';
import { Provider } from 'react-redux';
import store from './store/store';
import Wizard from './containers/Wizard';

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <div className="main-content">
          <div className="container">
            <div className="row">
              <Wizard />
            </div>
          </div>
        </div>
      </Provider>
    );
  }
}

export default App;