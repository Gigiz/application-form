const validate = values => {
  const errors = {};
  
  if(!values.note) {
    errors.note = 'Campo obbligatorio';
  }

  if(!values.attachment) {
    errors.attachment = 'Caricare il C.V.';
  } else if(values.attachment.name.indexOf('.pdf') === -1) {
    errors.attachment = 'Caricare solo pdf'
  }

  return errors;

};

export default validate;