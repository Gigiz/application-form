import React from 'react'
import { Field, reduxForm } from 'redux-form';
import CustomField, { FileInput } from '../CustomField';
import validate from './validate';

export const FormThree = (props) => {
    const { handleSubmit, previousPage } = props;
    return (
      <form onSubmit={handleSubmit}>
        <div>
          <span>Indicare qualche preferenza</span>
          <Field name="note" component={CustomField} type="textarea" />
          <Field name="attachment" component={FileInput} type="file"/>
        </div>
        <div className="form-buttons">
          <button type="button" className="btn btn-prev" onClick={previousPage}>Indietro</button>
          <button type="submit" className="btn btn-next">Candidati</button>
        </div>
      </form>
    )
}

export default reduxForm({
    form: 'wizard',
    destroyOnUnmount: false,
    forceUnregisterOnUnmount: true,
    validate,
})(FormThree);