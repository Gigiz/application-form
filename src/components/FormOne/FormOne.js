import React from 'react';
import { Field, reduxForm } from 'redux-form';
import validate from './validate';
import CustomField from '../CustomField';



const FormOne = props => {
  const { handleSubmit } = props;
  return(
    <form onSubmit={handleSubmit}>
      <fieldset>
        <Field name="firstName" type="text" component={CustomField} label="Nome" />
        <Field name="lastName" type="text" component={CustomField} label="Cognome" />
        <Field name="email" type="email" component={CustomField} label="E-Mail" />
        <Field name="age" type="date" component={CustomField} label="Data di nascita" />
        <Field name="sex" component={CustomField} type="radio" value="male" label="Maschio" />
        <Field name="sex" component={CustomField} type="radio" value="female" label="Femmina"/>
        <Field name="address" type="text" component={CustomField} label="Indirizzo" />
        <Field name="city" type="text" component={CustomField} label="Città" />
        <div className="form-buttons">
          <button type="submit" className="btn btn-next">Avanti</button>
        </div>
      </fieldset>
    </form>
  );
};

export default reduxForm({
  form: 'wizard',
  destroyOnUnmount: false,
  forceUnregisterOnUnmount: true,
  validate,
})(FormOne);
