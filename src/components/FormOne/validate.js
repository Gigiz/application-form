const validate = values => {
  const errors = {};
  
  if(!values.firstName) {
    errors.firstName = 'Campo obbligatorio';
  }

  if(!values.lastName) {
    errors.lastName = 'Campo obbligatorio';
  }

  if(!values.email){
    errors.email = 'Campo obbligatorio'
  }else if (values.email && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = 'E-Mail non valida';
  }

  if(!values.age) {
    errors.age = 'Campo obbligatorio';
  }

  if(!values.sex) {
    errors.sex = 'Campo obbligatorio';
  }

  if(!values.address) {
    errors.address = 'Campo obbligatorio';
  }

  if(!values.city) {
    errors.city = 'Campo obbligatorio';
  }

  return errors;

};

export default validate;