import React from 'react';
import './ProgressBar.css';

const lineStyle = [
  { width: '16%' },
  { width: '50%' },
  { width: '86%'},
  { width: '100%' },
]


const ProgressBar = props => {
  const style = lineStyle[props.page - 1];
  return(
    <div className="form-steps">
      <div className="form-progress">
        <div className="form-progress-line" data-now-value="16.66" data-number-of-steps="3" style={style}></div>
      </div>
      <div className="form-step active">
        <div className="form-step-icon"></div>
        <p>Chi sei</p>
      </div>
      <div className={ props.page >= 2 && "form-step active" || "form-step"}>
        <div className="form-step-icon"></div>
        <p>Cosa fai</p>
      </div>
      <div className={ props.page >= 3 && "form-step active" || "form-step"}>
        <div className="form-step-icon"></div>
        <p>Invia</p>
      </div>
      <div className={ props.page >= 4 && "form-step active" || "form-step"}></div>
    </div>
  );
};

export default ProgressBar;