import React from 'react';

const adaptFileEventToValue = delegate => e => delegate(e.target.files[0]);

export const FileInput = ({ 
  input: { value: omitValue, onChange, onBlur, ...inputProps }, 
  meta: omitMeta, 
  ...props 
}) => {
  return (
    <div>
      <label className="file-label">
        <span>Seleziona il tuo CV</span>
        <input
          onChange={adaptFileEventToValue(onChange)}
          onBlur={adaptFileEventToValue(onBlur)}
          type="file"
          accept="application/pdf"
          {...props.input}
          {...props}
        />
      </label>
      { !omitMeta.error && omitValue && <div>File Caricato: <span>{omitValue.name}</span></div>}
      { omitMeta && omitMeta.touched && omitMeta.error && <div>{omitMeta.error}</div>}
    </div>
  );
};