import React from 'react';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import './CustomField.css';
import 'react-datepicker/dist/react-datepicker.css';

const CustomField = ({ input, label, type, meta: {touched, error}, children }) => {
  switch(type) {
    case 'textarea':
      return (
        <div className="form-group">
          <textarea {...input} 
          type="text" 
          rows= "3"
          className={(touched && error && "form-control input-error") || "form-control"}
        >
        </textarea>
        </div>
      );
    case 'select':
      return(
        <div className="form-group">
          <select {...input}
            className={(touched && error && "form-control input-error") || "form-control"}
          >
            <option value="" disabled>{label}</option> 
            { children.map( option => <option value={option.value}>{option.label}</option>)}
          </select>
        </div>
      );
    case 'radio':
      return(
          <label className="radio inline">
            <input {...input} 
              type={type} 
              placeholder={label} 
              className={(touched && error && "form-control input-error") || "form-control"} 
            />
            <span htmlFor={input.name}>{label}</span>
          </label>
      );
    case 'date':
      return(
        <div className="form-input">
          <DatePicker {...input} 
            dateForm="DD/MM/YYYY"
            placeholderText={label}
            readOnly={true}
            minDate={moment("1970-01-01")}
            maxDate={moment()}
            selected={input.value ? moment(input.value) : null} 
            value={input.value ? moment(input.value) : null}
            className={(touched && error && "form-control input-error") || "form-control"}
            peekNextMonth
            showMonthDropdown
            showYearDropdown
            dropdownMode="select"
            withPortal
          />
        </div>
      );
    default:
      return(
        <div className="form-input">
          <label className="sr-only" htmlFor={input.name}>{label}</label>
          <input {...input} 
            type={type} 
            placeholder={label} 
            className={(touched && error && "form-control input-error") || "form-control"} />
        </div>
      );
  }
};

export default CustomField;