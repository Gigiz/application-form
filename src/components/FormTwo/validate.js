const validate = values => {
  const errors = {};
  
  if(!values.jobs) {
    errors.jobs = 'Seleziona una scelta';
  }

  return errors;

};

export default validate;