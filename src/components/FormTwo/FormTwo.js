import React from 'react';
import { Field, reduxForm } from 'redux-form';
import CustomField from '../CustomField'
import validate from './validate';

const mySelectOptions = [
  { value: 0, label: 'Full Stack Developer' },
  { value: 1, label: 'Product Manager' },
  { value: 2, label: 'Marketing Manager' },
];

const FormTwo = props => {
  const { handleSubmit, previousPage } = props;
  return(
    <form onSubmit={handleSubmit}>
      <fieldset>
        <Field name="jobs" type="select" component={CustomField} label="Seleziona una carriera">
          { mySelectOptions }
        </Field>
        <div className="form-buttons">
          <button type="button" className="btn btn-prev" onClick={previousPage}>Indietro</button>
          <button type="submit" className="btn btn-next">Avanti</button>
        </div>
      </fieldset>
    </form>
  );
};

export default reduxForm({
  form: 'wizard',
  destroyOnUnmount: false,
  forceUnregisterOnUnmount: true,
  validate,
})(FormTwo);
