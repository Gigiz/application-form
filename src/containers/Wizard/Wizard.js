import React, { Component } from 'react';
import FormOne from '../../components/FormOne';
import FormTwo from '../../components/FormTwo';
import FormThree from '../../components/FormThree';
import LandingPage from '../../components/LandingPage';
import ProgressBar from '../../components/ProgressBar';
import './Wizard.css';

const getBase64 = file => {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
  });
};
const serviceUrl = 'https://us-central1-rest-api-form.cloudfunctions.net/app/api/v1/apply';

class Wizard extends Component {
  constructor(props) {
    super(props);
    this.nextPage = this.nextPage.bind(this);
    this.previousPage = this.previousPage.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.state = {
      page: 1
    };
  }

  nextPage() {
    this.setState({ page: this.state.page + 1 });
  }

  previousPage() {
    this.setState({ page: this.state.page - 1 });
  }

  onSubmit(data) {
    let savingData = data;
    //console.log(savingData.attachment);
    getBase64(savingData.attachment).then(
      base64 => {
        console.log(base64);
        savingData.curriculum = {
          type: 'application/pdf',
          name: savingData.attachment.name,
          data: base64,
        };
        fetch(serviceUrl, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(savingData),
        })
        .then((response) => response.json())
        .then((responseJson) => {
          this.nextPage();
        })
        .catch((error) => {
          console.error(error);
        });
      }
    );
  }

  render() {
    const { page } = this.state;
    return(
      <div className="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3 form-box">
        <h3>Application Form</h3>
        <ProgressBar page={page} />
        {page === 1 && <FormOne onSubmit={this.nextPage} id="form-one" />}
        {page === 2 && <FormTwo onSubmit={this.nextPage} previousPage={this.previousPage} id="form-two" />}
        {page === 3 && <FormThree onSubmit={this.onSubmit} previousPage={this.previousPage} id="form-three" />}
        {page === 4 && <LandingPage id="landing-page"/>}
      </div>
    );
  }
};

export default Wizard;