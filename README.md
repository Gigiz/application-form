# Application Form

Applicazione creata partendo dal tool [Create React App][crapp]

[Live Demo][demo]

### Requisiti

  - [Node Js & Npm][nodejs]
  - [Yarn][yarn]
  - Browser Web

### How to

```sh
$ git clone git@bitbucket.org:Gigiz/application-form.git
$ cd application-form
$ yarn install
$ yarn start
```

### Todos
 - Landing page
 - Gestione errori http su salvataggio
 - Gestione Locale sul calendario


[crapp]: <https://www.npmjs.com/package/create-react-app>
[nodejs]: <https://nodejs.org/it/>
[yarn]: <https://yarnpkg.com/lang/en/>
[demo]: <https://application-form-ba88a.firebaseapp.com/>